<?php
$pdo = new PDO("mysql:host=localhost;dbname=global", "trole", "trole");
$pdo->query("SET NAMES UTF8");

$sql = "SELECT `id`, `name`, `author`, `year`, `isbn`, `genre` FROM books";
$arrHeader = array("id", "name", "author", "year", "isbn", "genre");

if (isset($_POST["submit"])) {
	if ((!empty($_POST["IBSN"]))||(!empty($_POST["NAME"]))||(!empty($_POST["AUTHOR"]))) {
		$sql = $sql . " WHERE ";
		if (!empty($_POST["IBSN"])) {
			$sql = $sql . " `isbn` like :IBSN ";
		};
		if (!empty($_POST["NAME"])) {
			$sql = $sql . ((!empty($_POST["IBSN"])) ? "AND" : "") . " `name` like :NAME ";
		};
		if (!empty($_POST["AUTHOR"])) {
			$sql = $sql . ((!empty($_POST["IBSN"])||(!empty($_POST["NAME"]))) ? "AND" : "") . " `author` like :AUTHOR ";
		}
	}
}

$sqlRes = $pdo->prepare($sql);
if (!empty($_POST["IBSN"])) {
	$sqlRes->bindValue(':IBSN',  "%" .   htmlspecialchars($_POST["IBSN"]) . "%" , PDO::PARAM_STR);
};
if (!empty($_POST["NAME"])) {
	$sqlRes->bindValue(':NAME', "%" .   htmlspecialchars($_POST["NAME"]) . "%", PDO::PARAM_STR);
}
if (!empty($_POST["AUTHOR"])) {
	$sqlRes->bindValue(':AUTHOR', "%" .  htmlspecialchars($_POST["AUTHOR"]) . "%" , PDO::PARAM_STR);
}
$sqlRes->execute();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>ДЗ 2.1 Установка и настройка веб-сервера</title>
	<style type="text/css">
	  form {
	    margin: 10px 0;	
	  }
		div {
			display: table;
		}
		ul {
			display: table-row;
		}
		li {
			display: table-cell;
			padding: 0 5px;
			vertical-align: middle;
			border: 1px solid;
		}
		.header {
			font-weight: 700;
			text-align: center;
			background-color: lightgrey;
		}
	</style>
</head>
<body>
	
	<h2>Список книг</h2>
	<form action="baseBook.php" method="post">
	  <label> Фильтр </label>
		<input type="text" name="IBSN" placeholder="ISBN" value=<?php if(isset($_POST["IBSN"])) { echo $_POST["IBSN"];};       	?>>
		<input type="text" name="NAME" placeholder="Название книги" value=<?php if (isset($_POST["NAME"])) { echo $_POST["NAME"];}; ?>>
		<input type="text" name="AUTHOR" placeholder="Автор книги" value=<?php if (isset($_POST["AUTHOR"])) { echo $_POST["AUTHOR"];}; ?> >
		<input type="submit" name="submit" value="Выбрать книги" />
	</form>

	<div>
		<ul>
			<?php
			foreach ($arrHeader as &$header) {	?>
			<li class="header"><?= $header ?></li>
			<?php	} ?>
		</ul>
		<?php
		foreach ($sqlRes as $item) {	
			?>
			<ul>
				<?php	foreach ($arrHeader as &$header) {

					?>
					<li><?= $item[$header] ?></li>	
					<?php  
				} 
				?>
			</ul>
			<?php	}	
			?>		
		</div>
	</body>

